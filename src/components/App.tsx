import React, { Component } from 'react'

import '../styles/main.css'

const greetings = (person: string): string => {
  return `Hello ${person}`
}

type Person = {
  id: number | undefined
}

const p1: Person = { id: 9 }

class App extends Component {
  render() {
    return (
      <div>
        <h1>Hello world!, this is React + TS {greetings('h')}</h1>
        <p>
          { p1.id }
        </p>
      </div>
    )
  }
}

export default App
